"use strict";

const fs = require('fs');

describe('File Operation', function () {

    it('read sample.txt', function () {
        const path = '4-practice/files/sample.txt';
        const content = fs.readFileSync(path, {encoding: 'utf8'});
        expect(content).toBe('' /* TODO: Change the value here to pass the test case */);
    });

    it('read CSV file', function () {

        // Test case "read sample.txt" above is the sample of how to read file via JavaScript in Node.js
        // Now try to read a CSV file by yourselves

        const path = '4-practice/files/profits.csv';

        const readFile = () => {
            // TODO: Read file in this function and return profits object
            return {
                'January': 78000
            }
        };

        const profits = readFile();
        expect(profits['January']).toBe(78000);
        expect(profits['February']).toBe(88000);
        expect(profits['March']).toBe(128000);
        expect(profits['April']).toBe(133000);

    });

});