"use strict";

describe('Primitive Types', () => {


    it('define a constant', () => {
        // TODO: define a constant
        expect(constant).toBe('Hello, world!');
    });

    it('define a variable', () => {
        // TODO: define a variable

        expect(variable).toBe('Hello, world!');

        // TODO: Reassign the variable to pass below checking
        expect(variable).toBe('Hello, earth!');
    });

    it('define different types of variable', () => {
        let variable = 'ABC';
        expect(variable).toBe('ABC');

        // TODO: Reassign the variable to pass below checking
        expect(typeof variable).toBe('number');
        expect(variable).toBe(123);

        // TODO: Reassign the variable to pass below checking
        expect(typeof variable).toBe('boolean');
        expect(variable).toBe(false);
    });

});